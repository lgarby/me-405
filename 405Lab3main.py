''' 
@file 405Lab3main.py

@brief Initializes adcDataCollection upon Start-up

@details Originally called main.py, this file runs adcDataCollection in an 
infinite loop upon Nucleo start-up in order to ensure that the nucleo is 
prepared to perform the ADC data collection tasks upon receival of the go
command from the front end.

@author Logan Garby
'''

from adcDataCollection import adcDataCollection

## Interval in ms to check for FrontEnd commands
interval = 5
pin = 'A5'

task = adcDataCollection(pin, interval)

while True:
    task.run()

