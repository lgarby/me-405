'''
@file main.py

@brief Main file for the Lab 9 tasks.

@details This file initializes two balancingPlatform.balancingPlatform 
        objects for each axis of the balancing board. The two tasks should
        be run cooperatively at an interval greater that the interval passed
        into the classes.

@author Logan Garby

@date 3/17/2021
'''

import utime
from balancingPlatform import balancingPlatform

xTask = balancingPlatform('x',10,1.5,0.8524,1,0.06845)
yTask = balancingPlatform('y',10,1.5,0.8524,1,0.06845)

xTask.Motor.extInt.disable()
xTask.Motor.enable()
xTask.Motor.extInt.enable()

interval = 50 
next_time = 0

while True:

    try:
        
        t = utime.ticks_ms()
        
        if t >= next_time:
            
            xTask.run()
            utime.sleep_ms(10)
            yTask.run()
            
        else:
            pass
        
        next_time = utime.ticks_add(t,interval)
        
    except:
        print('Task ended by exception')
        xTask.Motor.disable()
        break