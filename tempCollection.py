'''
@file tempCollection.py

@brief This file runs the mcp9808 driver and collects the data into a csv

@details 

@author Kai Quizon and Logan Garby

@date Feb 24th, 2021
'''
import utime
import pyb
from mcp9808 import mcp9808

## Initialize ADCAll Object
adc = pyb.ADCAll(12, 0x70000)

#Run a vref check to fix strange error with temp read
adc.read_vref()

## Initialize Data FileName
filename = 'tempData.csv'

## Initialize MCP9808 Driver
mcp9808 = mcp9808(24)

## Initial Time in integer for for algebra calcs
total_time = 0

with open(filename, 'w') as csv:
    header = 'Time (s), Time (min), Time (hr), Core Temperature (degF), Ambient Temperature (degF)\n'
    csv.write(header)
    
    # Only true for first run - used to make first time value = 0
    _first_run = True
            
    while True:     

        try:
            if _first_run == True:
                ## Sets start time using uticks
                start_time = utime.ticks_ms()
                
                ## Attribute for calculating time between runs
                last_time = start_time
                
                ## Temperature recording timestamp in seconds
                time_s = 0
                
                ## Temperature recording timestamp in minutes
                time_min = 0
                
                ## Temperature recording timestamp in hours
                time_hr = 0
                
                _first_run = False
            
            else:
                # Get Current Ticks
                time = utime.ticks_ms()
                
                # Calculate time from start
                dt = utime.ticks_diff(time, last_time)
                
                # Update Cycle Time
                last_time = time
                
                ## Time since start
                total_time += dt
                
                # Timestamp Updates
                time_s = total_time/1000      
                time_min = time_s/60
                time_hr = time_min/60
                    
            #Read Core Temp
            temp= adc.read_core_temp()
            tempF = temp*(9/5)+32
            
            #Read ambient temp off MCP9808
            temp_A = mcp9808.farenheit()
                
            ## Data string (changes every iteration)
            data = str(time_s) + ', ' + str(time_min) + ', '+str(time_hr) + ', ' + str(tempF) + ', ' + str(temp_A) + '\n'
            
            #Write data to csv file
            csv.write(data)
            
            #sleep until next second
            utime.sleep(60)
            
        except KeyboardInterrupt:
            
            print('Data Collection continued for ' + str(time_min) + ' minutes, or ' + str(time_hr) + ' hours.' )
            break
        
print('The .csv file has been closed and is stored on the Nucleo as ' + filename + '.')
            
            