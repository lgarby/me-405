%%%%    ME 405 Lab 6    %%%%

clear all
clc
close all

%% Constant Parameters

rm = 60; %[mm] Radius of Lever Arm 
lr = 50; %[mm] Length of Push Rod 
rB = 10.5; %[mm] Radius of Ball 
rG = 42; %[mm] Vertical Distance from U-Joint to CG of Platform 
lP = 110; %[mm] Horizontal Distance from U-Joint to Push-Rod Pivot
rP = 32.5; %[mm] Vertical Distance from U-Joint to Push-Rod Pivot 
rC = 50; %[mm] Vertical Distance from U-Joint to Platform Surface 
mB = 30; %[g] Mass of Ball 
mP = 400; %[g] Mass of Platform 
IP = 1.88*10^6; %[g*mm^2] Moment of Inertia of Platform (About Horizontal Axis through CG)
b = 10; %[ mNm*s/rad ]Viscous Friction at U-Joint 
IB = 2*mB*rB^2/5; %[g*mm^2] Inertia of Ball
g = 9.81; %[m/s^2]

%% Converting Parameters to SI Units

rm = rm/1000; %[m]
lr = lr/1000; %[m]
rB = rB/1000; %[m]
rG = rG/1000; %[m]
lP = lP/1000; %[m]
rP = rP/1000; %[m]
rC = rC/1000; %[m]
mB = mB/1000; %[kg]
mP = mP/1000; %[kg]
IP = IP/10^9; %[kg*m^2]
b = b/1000; %[Nm*s/rad]
IB = IB/10^9; %[kg*m^2]

%% Non-Linearized Matricies

syms x %[m]
syms x_dot %[m/s]
syms theta %[rad]
syms theta_dot; %[rad/s]
syms Tx %[N-m]

M11 = -(mB*rB^2+mB*rC*rB+IB)/rB; %[kg*m]
M12 = -(IB*rB+IP*rB+mB*rB^3+mB*rB*rC^2+2*mB*rB^2*rC+mP*rB*rG^2+mB*rB*x^2)/rB; %[kg*m^2];
M21 = -(mB*rB^2+IB)/rB; %[kg*m]
M22 = -(mB*rB^3+mB*rC*rB^2+IB*rB)/rB; %[kg*m^2] 

M = [M11 M12;M21 M22];

f1 = b*theta_dot - g*mB*(sin(theta)*(rB+rC)+x*cos(theta))+Tx*lP/rm + 2*mB*theta_dot*x*x_dot - g*mP*rG*sin(theta);
f2 = -mB*rB*x*theta_dot^2 - g*mB*rB*sin(theta);

f = [f1;f2];

%% Jacobian Linearization

g = M^-1*f;
g1 = g(1);
g2 = g(2);

g31 = diff(g1, x);
g32 = diff(g1, theta);
g33 = diff(g1, x_dot);
g34 = diff(g1, theta_dot);
g41 = diff(g2, x);
g42 = diff(g2, theta);
g43 = diff(g2, x_dot);
g44 = diff(g2, theta_dot);

u3 = diff(g1, Tx);
u4 = diff(g2, Tx);

x = 0;
x_dot = 0;
theta = 0;
theta_dot = 0;
Tx = 0;

A = [0 0 1 0
     0 0 0 1
     subs(g31) subs(g32) subs(g33) subs(g34)
     subs(g41) subs(g42) subs(g43) subs(g44)];
A= double(A);
 
B = double([0;0;subs(u3);subs(u4)]);

%% Simulations
% 
% x0 = [0;deg2rad(5);0;0];
% u = [0;0;0;0];
% C = eye(4);
% D = zeros(4,1);
% 
% K = [-.3 -.2 -.05 -.02 ];
% % K = zeros(1,4);
% imp = 0;
% 
% tsim = 20; %[s]
% sim('ME405_Lab6sim');
% 
% figure(1)
% 
% sgtitle('Closed Loop: Ball at 5cm Offset') 
% 
% subplot(2,2,1)
% plot(ans.tout, 1000*ans.x);
% title('Ball Position') 
% xlabel('Time [s]')
% ylabel('Position [mm]')
% 
% subplot(2,2,2)
% plot(ans.tout, ans.theta);
% title('Platform Angle') 
% xlabel('Time [s]')
% ylabel('Angle [rad]')
% 
% subplot(2,2,3)
% plot(ans.tout, 1000*ans.x_dot);
% title('Ball Velocity') 
% xlabel('Time [s]')
% ylabel('Velocity [mm/s]')
% 
% subplot(2,2,4)
% plot(ans.tout, ans.theta_dot);
% title('Platform Angular Velocity') 
% xlabel('Time [s]')
% ylabel('Angular Velocity [rad/s]')
% 
% width =900;
% height = 600;
% set(gcf,'position',[400,200,width,height])

%% Finding Controller Gains
close all

syms s K1 K2 K3 K4

K = [K1 K2 K3 K4];

A_CL = A - B*K;

P_s = det(s*eye(4)-A_CL);
P_s_coeffs = fliplr(coeffs(P_s,s));

zeta = 0.75;         % Chosen from ME 422 Manual, Figure 1A.1
t_s = 0.5;           % Settling Time in seconds
omega_n = 3*t_s/zeta;
far_poles = -20;      % Far double real axis pole location

P_s_picked = (s-far_poles)*(s+19) * (s^2 + 2*s*zeta*omega_n + omega_n^2);
P_s_picked_coeffs = fliplr(coeffs(P_s_picked,s));

eqns = [P_s_picked_coeffs(2)==P_s_coeffs(2)
    P_s_picked_coeffs(3)==P_s_coeffs(3)
    P_s_picked_coeffs(4)==P_s_coeffs(4)
    P_s_picked_coeffs(5)==P_s_coeffs(5)]';

K = solve(eqns,[K1 K2 K3 K4]);

charpoly

