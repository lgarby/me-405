    # Lab 3 - Encoder Driver

'''
@file Encoder_Driver.py

@brief Uses hardware to get encoder position and delta

@details This file uses the encoder count to calculate an accurate delta. 
The delta is then used to determine a position that is not affected
by overflow or underflow.

@author Logan Garby

@date 3/17/2021
'''

import utime
import pyb
from pyb import UART

class Encoder_Driver:
    '''
    Encoder Driver Class.
        
    This class reads the encoder hardware and returns the position and delta, 
    correcting for bad deltas appropriately. This can be used in conjunction 
    with Motor_Driver.py to control position of a motor.
        
    '''
        
    ## State 0 Constant
    S0_INIT = 0
    
    ## State 1 Constant
    S1_UPDATE = 1
    
    
    def __init__(self, interval, ch1_pin, ch1_timerchannel, ch2_pin, ch2_timerchannel, timer):
        ''' Creates encoder task objects. This class creates all the required objects for the 
        encoder driver, including pins, timers, and channels given by the input
        
        @param interval Interval in milliseconds
        @param ch1_pin Channel 1 Pin
        @param ch1_timerchannel Channel 1 Timer Channel
        @param ch2_pin Channel 2 Pin
        @param ch2_timerchannel Channel 2 Timer Channel
        @param timer Timer Number

        '''
        
        ## State Constant
        self.state = self.S0_INIT
        
        ## Start Time timestamp
        self.start_time = utime.ticks_ms()
        
        ## Interval in ms
        self.interval = int(interval)
        
        ## Next Time timestamp
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Timer designation
        self.tim = pyb.Timer(timer)
          
        # Setting Prescaler and Period
        self.tim.init(prescaler=0, period=0xFFFF)
        
        ## Pin object for channel 1
        self.ch1_pin = pyb.Pin(ch1_pin)
        
        ## Pin object for channel 2
        self.ch2_pin = pyb.Pin(ch2_pin)
        
        # Setting Timer Channel 1
        self.tim.channel(ch1_timerchannel, pin= self.ch1_pin, mode=pyb.Timer.ENC_AB)
        
        # Setting Timer Channel 2
        self.tim.channel(ch2_timerchannel, pin= self.ch2_pin, mode=pyb.Timer.ENC_AB)
        
        ## Encoder Count Variable
        self.count = 0
        
        ## Value of Previous Count
        self.previous_count = 0
        
        ## Encoder Position
        self.position = 0
        
        ## Previous Position
        self.previous_position = 0
        
        ## Encoder Delta
        self.delta = 0
        
        self.tim.counter(0)        
        
    def run(self):
        '''
        Runs one iteration of Task Encoder_Driver
        '''
        ## Current timestamp
        self.time = utime.ticks_ms()
        
        if utime.ticks_diff(self.time, self.next_time) >= 0:
            
            if (self.state == self.S0_INIT):
                self.transitionTo(self.S1_UPDATE)
                
            elif(self.state == self.S1_UPDATE):
                
                self.get_delta()
                self.get_position()
                
            else:
                pass
            
            # Next timestamp
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            
    
    def get_position(self):
        '''
        Determines the position based on the delta and previous position

        '''
        # Setting Previous Position
        self.previous_position = self.position
        
        # Updating Position
        self.position = self.previous_position + self.delta
        
        
    def get_delta(self):
        '''
        Gets an accurate delta accounting for overflow and underflow
        '''
        
        self.previous_count = self.count
        
        # Updates Count
        self.count = self.tim.counter()
        
        # Creates Delta
        self.delta = self.count - self.previous_count
        
        if self.delta > 32767.5:
            self.delta = self.delta - 65535
        
        elif self.delta < -32767.5:
            self.delta = self.delta + 65535
        
        else:
            pass
        
    def reset(self):
        '''
        Resets encoder values
        '''
        self.count = 0
        self.previous_count = 0
        self.position = 0
        self.previous_position = 0
        self.delta = 0
        self.tim.counter(0)
  
    
    def transitionTo(self, next_state):
        '''
        This method transitions to the next state
        @param next_state Designation for next state
        '''
        self.state = next_state
        
        
        
if __name__ == '__main__':
    '''
    This section tests the encoder driver for the balancing board encoders.
    '''
    task = Encoder_Driver(10, 'B6', 1, 'B7', 2, 4)
    
    uart = UART(2)
    
    cmd = 0
    
    stop = False
    
    next_time = utime.ticks_ms()
    
    while stop == False: 
        
        time = utime.ticks_ms()
        
        if utime.ticks_diff(time, next_time) >= 0:
            
            if uart.any() != 0:
                cmd = int(uart.readchar()) 
                
            else: 
                if cmd == 103 or cmd == 71:
                    task.run()
                    uart.write('Position: {}  |  Delta: {}\n'.format(task.position, task.delta))
                    
                elif cmd == 115 or cmd == 83:
                    uart.write('Stopped.')
                    Stop = True
        else:
            pass
        
        next_time = utime.ticks_add(time, 500)        
        
            
    