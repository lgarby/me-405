'''
@file adcDataCollection.py

@brief Contains the adcDataCollection class, must be run within a main file.

@details The class within this file is designed to be installed onto the Nucleo
and run in a continuous loop by a main file. 

@author Logan Garby

@date Feb 3rd, 2021
'''

import pyb
from pyb import UART
import utime
import array


class adcDataCollection:
    '''
    @brief Collects ADC data from the Nucleo and returns it to the FrontEnd.
    
    @details This class is a finite state machine which, upon arrival of the go
    command, builds an array of ADC data recorded by timer 6 every 1 millisecond.
    After 2 seconds, the data recording is completed, and the time and voltage 
    arrays is sent over the serial port for processing by the front end class
    titled adcFrontEnd.
    '''
    ## Initialization State
    S0_INIT = 0
    
    ## State 1 - Wait for Go
    S1_WAIT_FOR_GO = 1
    
    ## State 2 - Wait for Stop
    S2_WAIT_FOR_STOP = 2
    
    ## State 3 - Sending Data
    S3_SEND_DATA = 3

    def __init__(self, pin, interval):
        ''' Creates all objects belonging to the class. Includes the UART port,
        the pin for ADC, the data collection arrays, and the time attributes.
        
        @param pin ADC Input Pin Name
        @param interval Interval in ms for waiting for FrontEnd go command
        '''
        
        ## ADC Pin
        self.adcPin = pyb.Pin(pin)
        
        ## Serial Port Attribute
        self.myuart = UART(2)
        
        ## Time in milliseconds between runs
        self.interval = interval
        
        ## Command Attribute
        self.cmd = 0
        
        ## State Variable
        self.state = self.S0_INIT
        
        ## Start Time timestamp
        self.start_time = utime.ticks_ms()
        
        ## Next Time timestamp
        self.next_time = utime.ticks_add(self.start_time, self.interval)
            
        ## Position Array
        self.VoltageArray = array.array('H')
        
        ## Timestamp Array
        self.TimeArray = array.array('H')
        
    def run(self):
        '''
        Runs one iteration of the class adcDataCollection FSM
        '''

        ## Current Time timestamp
        self.time = utime.ticks_ms()
        
        if(self.state == self.S0_INIT):
            self.transitionTo(self.S1_WAIT_FOR_GO)
        else:
            pass
            
        if utime.ticks_diff(self.time, self.next_time) >= 0:
            '''
            Waits set interval
            '''
            
            if(self.state == self.S1_WAIT_FOR_GO):
                
                if self.myuart.any() != 0: 
                    '''
                    Checks for Character
                    '''
                    self.cmd = int(self.myuart.readchar())
                    # self.myuart.write(self.cmd)
                    if self.cmd == 103 or self.cmd == 71:
                    # if self.cmd == 1:
                        self.transitionTo(self.S2_WAIT_FOR_STOP)
                    else:
                        pass
                
                else:
                    pass
                    
            
            elif(self.state == self.S2_WAIT_FOR_STOP):
    

                adc = pyb.ADC(pyb.Pin.board.A5)    # create an ADC on pin A5
                tim = pyb.Timer(6, freq=1000)         # create a timer running at 1 ms
                
                self.VoltageArray = array.array ('H', (0 for index in range (2001)))    # completes after 2 s
                
                adc.read_timed(self.VoltageArray, tim)     
                
                n=0
                
                for n in range(len(self.VoltageArray)): # creates time array (0 - 2000 ms)
                    self.TimeArray.append(n)
                
                self.SEND() 
                
                self.transitionTo(self.S0_INIT)
                
            elif(self.state == self.S3_SEND_DATA):
                
                self.SEND()
                self.transitionTo(self.S0_INIT)
                
                
            else:
                pass
                
            ## Next Time timestamp
            self.next_time = utime.ticks_add(self.next_time, self.interval)
        
        else:
            pass
        

    def SEND(self):
        '''
        Runs a loop to write each row of the array to the serial port.
        '''
        n = 0
        
        self.myuart.write('{:}\r\n'.format(len(self.TimeArray)))
        
        for n in range(len(self.TimeArray)):
            self.myuart.write('{:}, {:}\r\n'.format(self.TimeArray[n], self.VoltageArray[n]))
            
        
    def transitionTo(self, next_state):
        '''
        This method transitions to the next state
        '''
        self.state = next_state


