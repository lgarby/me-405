var annotated_dup =
[
    [ "adcDataCollection", null, [
      [ "adcDataCollection", "classadcDataCollection_1_1adcDataCollection.html", "classadcDataCollection_1_1adcDataCollection" ]
    ] ],
    [ "adcFrontEnd", null, [
      [ "adcFrontEnd", "classadcFrontEnd_1_1adcFrontEnd.html", "classadcFrontEnd_1_1adcFrontEnd" ]
    ] ],
    [ "balancingPlatform", null, [
      [ "balancingPlatform", "classbalancingPlatform_1_1balancingPlatform.html", "classbalancingPlatform_1_1balancingPlatform" ]
    ] ],
    [ "Encoder_Driver", null, [
      [ "Encoder_Driver", "classEncoder__Driver_1_1Encoder__Driver.html", "classEncoder__Driver_1_1Encoder__Driver" ]
    ] ],
    [ "mcp9808", null, [
      [ "mcp9808", "classmcp9808_1_1mcp9808.html", "classmcp9808_1_1mcp9808" ]
    ] ],
    [ "Motor_Driver", null, [
      [ "Motor_Driver", "classMotor__Driver_1_1Motor__Driver.html", "classMotor__Driver_1_1Motor__Driver" ]
    ] ],
    [ "rtpDriver", null, [
      [ "rtpDriver", "classrtpDriver_1_1rtpDriver.html", "classrtpDriver_1_1rtpDriver" ]
    ] ],
    [ "thinkFast", null, [
      [ "thinkFast", "classthinkFast_1_1thinkFast.html", "classthinkFast_1_1thinkFast" ]
    ] ],
    [ "vendingMachine", null, [
      [ "vendingMachine", "classvendingMachine_1_1vendingMachine.html", "classvendingMachine_1_1vendingMachine" ]
    ] ]
];