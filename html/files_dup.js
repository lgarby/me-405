var files_dup =
[
    [ "405Lab3main.py", "405Lab3main_8py.html", "405Lab3main_8py" ],
    [ "adcDataCollection.py", "adcDataCollection_8py.html", [
      [ "adcDataCollection", "classadcDataCollection_1_1adcDataCollection.html", "classadcDataCollection_1_1adcDataCollection" ]
    ] ],
    [ "adcFrontEnd.py", "adcFrontEnd_8py.html", "adcFrontEnd_8py" ],
    [ "balancingPlatform.py", "balancingPlatform_8py.html", "balancingPlatform_8py" ],
    [ "Encoder_Driver.py", "Encoder__Driver_8py.html", "Encoder__Driver_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "mcp9808.py", "mcp9808_8py.html", "mcp9808_8py" ],
    [ "ME405_HW0x01.py", "ME405__HW0x01_8py.html", "ME405__HW0x01_8py" ],
    [ "Motor_Driver.py", "Motor__Driver_8py.html", "Motor__Driver_8py" ],
    [ "rtpDriver.py", "rtpDriver_8py.html", "rtpDriver_8py" ],
    [ "tempCollection.py", "tempCollection_8py.html", "tempCollection_8py" ],
    [ "thinkFast.py", "thinkFast_8py.html", "thinkFast_8py" ],
    [ "vendingMachine.py", "vendingMachine_8py.html", "vendingMachine_8py" ]
];