/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 405 Documentation Portfolio", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Labs", "index.html#Labs", null ],
    [ "Homework", "index.html#HW", null ],
    [ "Lab 0x01 - Reintroduction to Python and Finite State Machines", "Lab1.html", [
      [ "Description", "Lab1.html#l1desc", null ],
      [ "Source Code", "Lab1.html#l1sc", null ],
      [ "Finite State Machine Diagram", "Lab1.html#FSM1", null ]
    ] ],
    [ "Lab 0x02 - Think Fast!", "Lab2.html", [
      [ "Description", "Lab2.html#l2desc", null ],
      [ "Source Code", "Lab2.html#l2sc", null ]
    ] ],
    [ "Lab 0x03 - Pushing the Right Buttons", "Lab3.html", [
      [ "Description", "Lab3.html#l3desc", null ],
      [ "Links to File Documentation:", "Lab3.html#l3files", null ],
      [ "Links to Class Documentation:", "Lab3.html#l3classes", null ],
      [ "Source Code", "Lab3.html#l3sc", null ]
    ] ],
    [ "Lab 0x04 - Hot or Not?", "Lab4.html", [
      [ "Description", "Lab4.html#l4desc", null ],
      [ "Collaberation on Lab 4", "Lab4.html#colab", null ],
      [ "Hyperlinks to the Lab 4 File and Class Documentation", "Lab4.html#l4files", null ],
      [ "Shared Repository Source Code Links", "Lab4.html#l4sc", null ],
      [ "Temperature Collection Results", "Lab4.html#png4", null ]
    ] ],
    [ "Lab 0x05 - Getting Tipsy", "Lab5.html", [
      [ "Description", "Lab5.html#l5desc", null ],
      [ "Reference Diagrams", "Lab5.html#l5ref", null ],
      [ "Hand Calculations", "Lab5.html#l5hc", null ]
    ] ],
    [ "Lab 0x06 - Simulation or Reality?", "Lab6.html", [
      [ "Description", "Lab6.html#l6desc", null ],
      [ "Links to MATLAB Source Code", "Lab6.html#l6sc", null ],
      [ "Results", "Lab6.html#l6r", null ]
    ] ],
    [ "Lab 0x07 - Feeling Touchy", "Lab7.html", [
      [ "Description", "Lab7.html#lab7desc", null ],
      [ "Documentation Hyperlinks", "Lab7.html#l7hls", null ],
      [ "Link to Source Code", "Lab7.html#l7sc", null ],
      [ "Example of REPL Results", "Lab7.html#l7ex", null ],
      [ "Images of the Lab Setup", "Lab7.html#l7images", null ]
    ] ],
    [ "Lab 0x08 - Term Project I", "Lab8.html", [
      [ "Description", "Lab8.html#l8desc", null ],
      [ "Documentation Hyperlinks", "Lab8.html#l8hls", null ],
      [ "Source Code", "Lab8.html#l8sc", null ],
      [ "Image of Setup", "Lab8.html#l8pic", null ]
    ] ],
    [ "Lab 0x09 - Term Project II", "Lab9.html", [
      [ "Description", "Lab9.html#l9desc", null ],
      [ "Link to Video", "Lab9.html#l2v", null ],
      [ "Documentation Hyperlinks", "Lab9.html#l9hls", null ],
      [ "Links to Shared Repository Source Code", "Lab9.html#l9sc", null ]
    ] ],
    [ "Homework 0x01 - Python Review (Functions/Tuples)", "HW0.html", [
      [ "Description", "HW0.html#hw0desc", null ],
      [ "Source Code", "HW0.html#h1sc", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"",
"main_8py.html#ae817fc8c30b33314714620d73c36bcc6"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';