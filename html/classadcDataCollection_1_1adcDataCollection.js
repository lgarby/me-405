var classadcDataCollection_1_1adcDataCollection =
[
    [ "__init__", "classadcDataCollection_1_1adcDataCollection.html#aa32eb171e8751dc2214dc8214be10ce1", null ],
    [ "run", "classadcDataCollection_1_1adcDataCollection.html#a54c57a83fb331386f93d2e2489fa91af", null ],
    [ "SEND", "classadcDataCollection_1_1adcDataCollection.html#a48aa817625fe7c8f5dbfdc82c5b9f119", null ],
    [ "transitionTo", "classadcDataCollection_1_1adcDataCollection.html#a4c1b42b40ae8dfc82a9601f625f9f092", null ],
    [ "adcPin", "classadcDataCollection_1_1adcDataCollection.html#abeb0e72134d324904731564c605a4320", null ],
    [ "cmd", "classadcDataCollection_1_1adcDataCollection.html#a0ed8fe58e5a045880112b04be88195ce", null ],
    [ "interval", "classadcDataCollection_1_1adcDataCollection.html#aba6505173b9d600703807680f4ea5822", null ],
    [ "myuart", "classadcDataCollection_1_1adcDataCollection.html#a62647eb51ddcc5d884546d8b69a0bfc1", null ],
    [ "next_time", "classadcDataCollection_1_1adcDataCollection.html#a4d2ecbef7ca0f65885947840d46f880a", null ],
    [ "start_time", "classadcDataCollection_1_1adcDataCollection.html#af5137884da2b538cb297f9e0b021258f", null ],
    [ "state", "classadcDataCollection_1_1adcDataCollection.html#a56b60eda4a18bc21c604c24d6e8d56dd", null ],
    [ "time", "classadcDataCollection_1_1adcDataCollection.html#abeff4218537ceead88f432407f8addb5", null ],
    [ "TimeArray", "classadcDataCollection_1_1adcDataCollection.html#a8bacf18183670ff2f91af110b1a42ba2", null ],
    [ "VoltageArray", "classadcDataCollection_1_1adcDataCollection.html#a00566dd5cf6708743327d795200d675f", null ]
];