var lab0x03share_8py =
[
    [ "csv_omega", "lab0x03share_8py.html#a9beef128f8d57dd331d0ea0a39868fa6", null ],
    [ "csv_position", "lab0x03share_8py.html#a40dec9a3891b76ae57c188b4bd220b53", null ],
    [ "csv_trigger", "lab0x03share_8py.html#a12a27e4b01ce7e18e686cadb1ca9dc27", null ],
    [ "duty", "lab0x03share_8py.html#a5c3fc7f4dfddb5fe02d999b6ac5fa06a", null ],
    [ "integral_err", "lab0x03share_8py.html#a88edb1e3989c175e03da1f0b87b179da", null ],
    [ "Motor_state", "lab0x03share_8py.html#a66c5776745bbd51a83c05478eb87a78a", null ],
    [ "omega_real", "lab0x03share_8py.html#aa0a7ff64367a1e8f97996a699e61a348", null ],
    [ "position", "lab0x03share_8py.html#a60ef3f13fa8487a8708cec8b58ff9167", null ],
    [ "prev_duty", "lab0x03share_8py.html#a77fafdd2c916c7cdb31baf4ff5040821", null ],
    [ "prev_position", "lab0x03share_8py.html#a94d46f79e68972fbfe88f73653dcbeef", null ],
    [ "raw_delta", "lab0x03share_8py.html#a81b393230c6ecab94f73904279e0eda3", null ],
    [ "raw_pos", "lab0x03share_8py.html#a6eb3d43ba7f68a00a942faa989a0283b", null ],
    [ "real_delta", "lab0x03share_8py.html#aba343c7126a42fb50029fd26a610a11e", null ],
    [ "rolls", "lab0x03share_8py.html#a3cf74dc94b1655e264449304407e5932", null ],
    [ "tim", "lab0x03share_8py.html#a780b8d5f208858102d6228aba4b9966a", null ]
];