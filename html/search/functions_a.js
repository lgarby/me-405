var searchData=
[
  ['reactiontime_199',['reactionTime',['../classthinkFast_1_1thinkFast.html#a7624df2c5ffa044a05e468e30a4bb62c',1,'thinkFast::thinkFast']]],
  ['reset_200',['reset',['../classEncoder__Driver_1_1Encoder__Driver.html#a62b9bc1f4e82b4981494446068cf3a19',1,'Encoder_Driver.Encoder_Driver.reset()'],['../classMotor__Driver_1_1Motor__Driver.html#af291e40b88aef038e8eacc8961b33b22',1,'Motor_Driver.Motor_Driver.reset()']]],
  ['resetfun_201',['resetFun',['../classthinkFast_1_1thinkFast.html#a37dcabe84e903a2af01e458b46df8658',1,'thinkFast::thinkFast']]],
  ['run_202',['run',['../classadcDataCollection_1_1adcDataCollection.html#a54c57a83fb331386f93d2e2489fa91af',1,'adcDataCollection.adcDataCollection.run()'],['../classadcFrontEnd_1_1adcFrontEnd.html#a1d0b5c8b00bc448160b93acf6248ec41',1,'adcFrontEnd.adcFrontEnd.run()'],['../classbalancingPlatform_1_1balancingPlatform.html#a813386a902cc9dc95fb423e0a8468f81',1,'balancingPlatform.balancingPlatform.run()'],['../classEncoder__Driver_1_1Encoder__Driver.html#a469f85686c29975f8b8709cf5e44afd7',1,'Encoder_Driver.Encoder_Driver.run()'],['../classvendingMachine_1_1vendingMachine.html#ace5d9d23edb0f1c17cfaba9db16cca37',1,'vendingMachine.vendingMachine.run()']]]
];
