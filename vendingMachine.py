'''
@file vendingMachine.py

@brief Contians and runs a FSM for vending machinge operation.

@details This file performs the basic operations of a virtual vending machine. It accepts payment in the form of numbers 0-7 corresponding
with coins and bills and when the user selects a product, it either vends and subtracts from their balance or lets them know their funds were
not enough. The balance can be ejected at anytime throughout the FSM. While running in the vendingMachine.py file, the class runs on an infinite
while loop that requires the user to Keyboard Interrupt to stop.

@author Logan Garby

@date January 21, 2020

'''

import keyboard


def on_keypress (thing):
     ''' 
     Callback which runs when the user presses a key.
     '''
     global pushed_key
     
     pushed_key = thing.name

        

class vendingMachine:
    '''
    @brief One iteration of the FSM that controls the vending machine
    
    @details contains the method run() to be looped continuously. The keyboard module
    is required to ensure keypresses are registered without the use of blocking code. 
    Global variable pused_key is also used through the class to ensure keyboard has 
    a way to communicate to the class.
    '''
    ## State 0 attribute equal to 0
    S0_INIT = 0
    
    ## State 1 attribute equal to 1
    S1_PAYMENT = 1
    
    ## State 2 attribute equal to 2
    S2_CHECK_FUNDS = 2
    
    ## State 3 attribute equal to 3
    S3_VENDING = 4
    
    ## Cuke price in cents
    cuke_price = 100
    
    ## Popsi price in cents
    popsi_price = 120
    
    ## Spryte price in cents
    spryte_price = 85
    
    ## Dr.Pupper price in cents
    drpupper_price = 110


    def __init__(self):
        '''
        Creates objects required for the vendingMachine class.
        '''
        ## State Variable
        self.state = self.S0_INIT

        ## Attribute for the entered key
        self.key = None
        
        ## Total entered balance
        self.balance = float(0)

        global pushed_key
        pushed_key = None
 
    def run(self):
        '''
        Runs one iteration of the vendingMachine FSM
        '''
             
        
        if(self.state == self.S0_INIT):
        
            print('Press keyboard to insert change: \n'
                  '0 = penny\n'
                  '1 = nickle\n'
                  '2 = dime\n'
                  '3 = quarter\n'
                  '4 = dollar\n'
                  '5 = 5 dollars\n'
                  '6 = 10 dollars\n'
                  '7 = 20 dollars\n'
                  
                  'Press "c" for Cuke, "p" for Popsi, "d" for Dr. Pupper, "s" for Spryte, and "e" to Eject')
        
            # Sets callback
            keyboard.on_press(on_keypress)
            
            self.transitionTo(self.S1_PAYMENT)
                
        if(self.state == self.S1_PAYMENT):
            '''
            State 1 tallies the payment while waiting on a soda selection to transition to state 2
            '''
            # Sets self.key to the global pushed_key
            global pushed_key
            self.key = pushed_key
            
            if self.key:
                
                # Checking whether a number or letter was pushed
                try:
                    self.key = int(self.key)
                except:
                    if self.key == 'e':
                        self.ejectChange()
                    elif self.key == 'c' or self.key == 's' or self.key == 'p' or self.key == 'd':
                        self.transitionTo(self.S2_CHECK_FUNDS)
                    else:
                        print('ERROR - INVALID CHARACTER')           
                    
                else:
                    self.countPayment(self.key)
                    print('Your balance is: {:}'.format(self.balance/100))
            else:
                pass
            
            # Resets global variable to wait for new input
            pushed_key = None


        if(self.state == self.S2_CHECK_FUNDS):
            '''
            Once a soda is selected, state 2 checks the funds required and resets the key attribute
            '''
            self.checkFunds(self.key)
            
            self.key = None
            
            
        if(self.state == self.S3_VENDING):
            '''
            If funds are sufficient for a soda, state 3 checks if the user wants to continue purchasing
            '''
            self.key = pushed_key
            
            if self.key:
                
                if self.key == 'y':
                    print('Your balance is: {:}'.format(self.balance/100))
                    self.transitionTo(self.S1_PAYMENT)
                if self.key == 'n':
                    self.ejectChange()
                    self.transitionTo(self.S0_INIT)
            
            pushed_key = None


    def countPayment(self, key):  
        ''' Counts coins inserted so far.
        Takes the most recent key that has been pressed and adds that coin value to the balance. 
        If an integer 0-7 is not pressed, the method will print "Error - Invalid Key"
        
        @param key The newest key pressed, to be added to the balance.
        '''
        if key == 0:
            self.balance += 1
        elif key == 1:
            self.balance += 5
        elif key == 2:
            self.balance += 10
        elif key == 3:
            self.balance += 25
        elif key == 4:
            self.balance += 100
        elif key == 5:
            self.balance += 500
        elif key == 6:
            self.balance += 1000
        elif key == 7:
            self.balance += 2000
        else:
            print('Error - Invalid Key')
            
            
    
    ## Checks if the balance is enough for the purchase.
    #
    # If the balance is enough, the FSM transitions to the vending state, and the 
    # chosen soda is shown to be dispensed
    #
    # @param key Key pressed containing soda choice
    def checkFunds(self, key):
        
        disp = True
        
        if key == 'c' and self.balance >= self.cuke_price:
            self.balance -= self.cuke_price
            print('Cuke Dispensed')
            
        elif key == 'p' and self.balance >= self.popsi_price:
            self.balance -= self.popsi_price
            print('Popsi Dispensed')

        elif key == 's' and self.balance >= self.spryte_price:
            self.balance -= self.spryte_price
            print('Spryte Dispensed')

        elif key == 'd' and self.balance >= self.drpupper_price:
            self.balance -= self.drpupper_price
            print('Dr. Pupper Dispensed')         
        
        else:
            self.transitionTo(self.S1_PAYMENT)
            print('Insufficient Funds')
            disp = False
        
        if disp == True:
            self.transitionTo(self.S3_VENDING)
            print('Purchase another item? (y/n)')
        else:
            pass
        
        
        
    def ejectChange(self):
        ''' Method for ejecting change. When called, ejectChange() prints the amount that the user had as their balance and 
        resets the balance back to zero. The FSM then returns to the initialization state
        to wait for the next user
        '''
        
        print('Balance of {:} Returned'.format(self.balance/100))
        self.balance = 0
        self.transitionTo(self.S0_INIT)
        
        
        
    def transitionTo(self, next_state):
        '''
        This method transitions to the next state
        '''
        self.state = next_state        


if __name__ == '__main__':    
    
    task = vendingMachine()
    
    while True:
        task.run()

