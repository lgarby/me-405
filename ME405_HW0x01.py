'''
@file ME405_HW0x01.py
@brief Python review of tuples and functions
@details This function recieves a price and tuple corresponding to different
coins for payment and outputs a tuple, in the same format, for the change.
@author Logan Garby
@date January 13th, 2020
'''

def getChange(price, payment):
    '''
    getChange takes the price and payment input parameters and returns a tuple 
    of the appropriate change.
    
    @param price Cost of item in cents (int)
    @param payment Tuple of payment in the form (pennies, nickles, dimes, quarters, ones, fives, tens, twenties)
    '''
    payment_cents = payment[0]+5*payment[1]+10*payment[2]+25*payment[3]+100*payment[4]+500*payment[5]+1000*payment[6]+2000*payment[7]
    
    change_cents = payment_cents-price
    
    c20 = 0
    c10 = 0
    c5 = 0
    c1 = 0
    cq = 0
    cd = 0
    cn = 0
    cp = 0
    
    
    while change_cents != 0:
        
        if change_cents >= 2000:
            c20 += 1
            change_cents = change_cents - 2000
        
        elif change_cents >= 1000:
            c10 += 1
            change_cents = change_cents - 1000
        
        elif change_cents >= 500:
            c5 += 1
            change_cents = change_cents - 500
        
        elif change_cents >= 100:
            c1 += 1
            change_cents = change_cents - 100
        
        elif change_cents >= 25:
            cq += 1
            change_cents = change_cents - 25
        
        elif change_cents >= 10:
            cd += 1
            change_cents = change_cents - 10
        
        elif change_cents >= 5:
            cn += 1
            change_cents = change_cents - 5
        
        elif change_cents >= 1:
            cp += 1
            change_cents = change_cents - 1
        
    change = (cp,cn,cd,cq,c1,c5,c10,c20)
            
    
    
    print(change)



if __name__ == "__main__":
    
    payment = (3,0,0,2,1,0,0,1)
    price = 1573
    
    getChange(price, payment)
    
    