'''
@file adcFrontEnd.py

@brief Contains the adcFrontEnd class and a main file to continuously run it.

@details In order for the finite state machine in adcFrontEnd to work properly,
this class must be looped in a continuous while loop that only ends when the 
self.stop attribute is changed to TRUE once the code is completed.

@author Logan Garby

@date Feb 3rd, 2021
'''

import serial
import matplotlib.pyplot as plt
import numpy as np
import csv

# Initializing Serial Port
ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)

class adcFrontEnd:
    '''
    @brief Sends start cmd to adcDataCollection, builds .csv, and plots data.
    
    @details This class prompts the user to press 'g' to start recoding data from 
    the ADC reader connected to the user button outlined in the class adcDataCollection.
    After the go command is given, adcFrontEnd waits for the data to be returned through
    the serial port by the Nucleo before building a .csv file and plotting the data.
    '''
    
    ## Initialization State Value
    S0_INIT = 0
    
    ## Wait for Go State Value
    S1_WAIT_FOR_GO = 1
    
    ## Read Data State Value
    S2_READ_DATA = 2
    
    def __init__(self):
        '''
        Creates all objects needed for the class adcFrontEnd
        '''
        
        ## User Inputed Attribute
        self.inv = ''
       
        ## State Variable
        self.state = self.S0_INIT
        
        ## Data from Serial Port
        self.myval = ''
        
        ## Boolean to Stop Recording
        self.stop = False
        
        ## Number of rows sent from Nucleo
        self.n = 0
        
        ## Time Array
        self.time = np.array([])
        
        ## Voltage Array
        self.voltage = np.array([])
         
    
    def run(self):
        '''
        Runs one iteration of an FSM for promting the user and outputing data
        '''
        
        if(self.state == self.S0_INIT):
            self.inv = input("Press 'G' to start recording: ")
            self.transitionTo(self.S1_WAIT_FOR_GO)
            
        elif(self.state == self.S1_WAIT_FOR_GO):
            
            if self.inv == 'g' or self.inv == 'G':
                ser.write(str(self.inv).encode('ascii'))
                self.transitionTo(self.S2_READ_DATA)
                
            else:   
                print('Error - Invalid Character')
                self.inv = input("Press 'G' to start encoding: ")
            

        elif(self.state == self.S2_READ_DATA):
            
            if ser.in_waiting != 0:
                self.n = int(ser.readline().decode('ascii'))
                
                # Index Attribute for While loop
                idx = 1
                
                while idx <= self.n:
                    self.myval = ser.readline().decode('ascii')
                    
                    # Attribute for Serial Port Values
                    vals = self.myval.split(", ")
                    
                    if idx == 1:

                        self.makeCSV(float(vals[0])/1000, 3.3*float(vals[1])/4095)
                        
                    else:

                       self.buildCSV(float(vals[0])/1000, 3.3*float(vals[1])/4095)
                    
                    self.time = np.append(self.time, float(vals[0])/1000)
                    self.voltage = np.append(self.voltage, 3.3*float(vals[1])/4095)
                    
                    idx += 1
                    
                self.stop = True
                
                self.plotData()
            
            else:
                pass
        

    def transitionTo(self, next_state):
        '''
        This method transitions to the next state.
        '''
        self.state = next_state
       
        
    def plotData(self):
        '''
        This method plots the data from the ADC pin.
        '''
        plt.plot(self.time, self.voltage)
        plt.ylabel('User Button Voltage [V]')
        plt.xlabel('Time [s]')
        plt.ylim([0,4])
        plt.show()
        
        
    def makeCSV(self, t, v):
        '''
        Creates new CSV file or clears the old one.
        '''
        with open("adcData.csv",'w', newline='') as f:
         writer = csv.writer(f,delimiter=",")
         writer.writerow(['Time [s]','Voltage [V]'])
         writer.writerow([t,v])
        
        
    def buildCSV(self, t, v):
        ''' Creates ADC Data CSV. By being continually called every time new
        data is to be inputed, this methods builds the csv row by row.
        '''
        with open("adcData.csv",'a',newline='') as f:
         writer = csv.writer(f,delimiter=",")
         writer.writerow([t,v])
    
    
if __name__ == '__main__':  
    
    task = adcFrontEnd()
    
    while task.stop == False:
        task.run()
        # try:
        #     task.run()
        # except:
        #     ser.close()
        #     print('ERROR')
        #     break
            
    ser.close()
