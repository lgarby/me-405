'''
@file mcp9808.py

@brief This file contains a driver for the mcp9808 temperature sensor.

@details The following file contains a class called mcp9808.mcp9808 which
    creates the i2c communication required to run the adafruit mcp9808
    temperature sensor.

@author Kai Quizon and Logan Garby

@date Feb 24th, 2021
'''

from pyb import I2C
import utime

class mcp9808:
    '''
    @brief Driver for I2C adaFruit mcp9808 temperature sensor.
    
    @details This class contains methods to check if the I2C communication has
    been set up properly, read the temperature in celcius, and read the
    temperature in fahrenheit.
    '''
    # def __init__(self, address):
        
    #     self.address = address
    #     self.I2C = pyb.i2c(1, I2C.MASTER)
    def __init__(self, address):
        '''
        @brief Initiates the class by storing a copy of desired address and intiating I2C communication
        @param address The Device address for the connected MCP9808
        '''
        ## Create I2C on Bus 1 and initiate as Master
        self.i2c = I2C(1, I2C.MASTER)
        ## I2C Address inputed when class is initialized
        self.addr = address
        
    def check(self):
        '''
        @brief This function checks whether the read value matches the Manufacturer ID of MCP9808.
        @return A boolean True or False. True if check passes, False if check fails.
        '''
        ## Buffer Array to write from Manufacturer ID Register
        checkbuf = bytearray(2)
        
        #Write into checkbuf from manufacturer ID register
        self.i2c.mem_read(checkbuf, addr = self.addr, memaddr = 6)
        
        #Check if Published Manufacturer ID matches received ID
        if checkbuf[1] == 84:
            return True
        else:
            return False
        
    def celsius(self):
        '''
        @brief Returns the temperature read by the MCP9808 in Degrees Celsius
        @return A float (decimal) value for the temperature read in Degrees Celsius.
        '''
        ## Buffer Array to write Temperature Data Into
        buf = bytearray(2)
        
        #Write Temperature Data to buf from corresponding register on MCP9808
        self.i2c.mem_read(buf, addr = self.addr, memaddr = 5)
        
        #Clear Flags
        buf[0] = buf[0] & 0x1F
        # Check if data is below 0*C
        if buf[0] & 0x10 == 0x10:
            # If data is below 0*C, clear sign and "resign" in integer calc
            buf[0] = buf[0] & 0x0F
            temp = (buf[0]*16+buf[1]/16)-256   #Converts to temperature in decimal
        else:    
            # Runs if data indicates above 0*C
            temp = buf[0]*16+buf[1]/16      #Converts to temperature in decimal
        return temp
    
    def fahrenheit(self):
        '''
        @brief Returns the temperature read by the MCP9808 in Degrees Farenheit.
        @return A float (decimal) value for the temperature read in Degrees Farenheit.
        '''
        buf = bytearray(2)
        self.i2c.mem_read(buf, addr = self.addr, memaddr = 5)
        buf[0] = buf[0] & 0x1F
        # Check if data is below 0*C
        if buf[0] & 0x10 == 0x10:
            # If data is below 0*C, clear sign and "resign" in integer calc
            buf[0] = buf[0] & 0x0F
            temp = (buf[0]*16+buf[1]/16)-256
            
        else:    
            # Runs if data indicates above 0*C
            temp = buf[0]*16+buf[1]/16
        tempF = temp*(9/5)+32
        return tempF
    
    
if __name__ == '__main__':
    #This function returns the temperature in degrees celsius every second (if the driver is running correctly)
    mcp = mcp9808(24)
    #Check that Manufacturer ID matches published.
    mcp.check()
    while True:
        print(mcp.celsius())
        utime.sleep(1)
        
        
