'''
@file balancingPlatform.py

@brief One iteration of platform balancing, closed-loop control.

@details This file calls Encoder_Driver.py, Motor_Driver.py, and rtpDriver.py to
obtain the necessary values for a closed loop feedback control system aimed at 
balancing the steel and rubber ball on the resistive touch panel platform.

@author Logan Garby

@date 3/17/2021
'''

from Motor_Driver import Motor_Driver
from Encoder_Driver import Encoder_Driver
from rtpDriver import rtpDriver
import utime
import math

class balancingPlatform:
    '''
    @brief Runs one iteration of a closed loop control system
    
    @details This class initializes a Motor, Encoder and rtp object to obain 
    a closed-loop feedback response for the platform and ball balancing.
    '''   
    
    pi = math.pi
    
    def __init__(self,axis,interval,K1,K2,K3,K4):
        ''' Initializing the objects used for this class. Includes the x and theta
        states, controller gains, and motor properties. In addition, the initalization
        of this class ask for the user to choose a motor axis and initializes the 
        correct motors and encoders and detects the appropiate rtp positions.
        
        @param axis Axis for balancing task (either 'x' or 'y') [string]
        @param interval Millisecond interval at which the encoder and rtp take measurements 
            for ball velocity and platform angular velocity [integer]
        @param K1 Ball position torque output gain [float]
        @param K2 Platform angle output gain [float]
        @param K3 Ball velocity torque output gain [float]
        @param K4 Platfrom angular velocity output gain [float]
        
        '''
        
        ## Motor Torque Output Gains 
        self.K = [K1, K2, K3, K4]
        
        ## Resistive touch panel measured ball position (x) [m]
        self.x = 0
        
        ## Resistive touch panel measured ball velocity (x dot) [m/s]
        self.x_dot = 0
        
        ## Encoder measured platform angle (theta) [rad]
        self.theta = 0
        
        ## Encoder measured platform angular velocity (theta dot) [rad/s]
        self.theta_dot = 0
        
        ## Interval that the encoder and rtp sensor are run at [millisecond integer]
        self.Interval = 0
        
        ## Motor Torque Attribute
        self.Tm = 0
        
        ## Duty cycle attribute
        self.L = 0 
        
        ## Integer object used for logic associated with axis selection [1 for x, 2 for y]
        self.axis = 0 
        
        ## Encoder 1 Object
        self.Enc1 = Encoder_Driver(0,'B6',1,'B7',2,4)

        ## Encoder 2 Object
        self.Enc2 = Encoder_Driver(0,'C6',1,'C7',2,8)
    
        ## RTP object
        self.rtp = rtpDriver('A1','A7','A0','A6',21,32.5,2014,1920)
        
        if axis == 'x':
            ## Motor object being used for this axis
            self.Motor = Motor_Driver('A15','B2','B4',1,'B5',2,3)
            self.Motor.set_duty(0)
            self.axis = 1
            ## Encoder object being used for this axis
            self.Enc = self.Enc1
            # self.Motor.extInt.disable()
            # self.Motor.enable()
            # utime.sleep_ms(20)
            # self.Motor.extInt.enable()
            
        elif axis == 'y':
            # Motor 2 Object
            self.Motor = Motor_Driver('A15','B2','B0',3,'B1',4,3)
            self.Motor.set_duty(0)
            self.axis = 2
            self.Enc = self.Enc2
            
        else:
            print("Error - Incorrect first parameter (axis) - Enter 'x' or 'y'")
         
        ## RTP and Encoder velocity measurement interval (ms)
        self.interval = interval
        

    def run(self):
        ''' Runs one loop of the closed loop control system. This function is the 
        only method that needs to be called continuously for the system to 
        behave as expected. The interval at which this is called should be
        greater than the interval passed into this class object.
        '''
        self.get_States()
        print('axis: {:}'.format(self.axis))
        print('{:}, {:}, {:}, {:}'.format(self.x, self.theta, self.x_dot, self.theta_dot))
        
        self.Tm = -(self.K[0]*self.x + self.K[1]*self.theta + 
                    self.K[2]*self.x_dot + self.K[3]*self.theta_dot)
        
        print(str(self.Tm))
        
        self.L = self.Tm*2.21*100*1000/(12*13.8*4) 
        
        print(str(self.L))
        
        self.Motor.set_duty(self.L)
        
        print('Duty set to {:}'.format(self.L))
        
    def get_rtpData(self):
        ''' This method calcuates the ball position and velocity. It uses the
        resistive touch panel to scan the relevant axis for the task. It then 
        waits 10 ms and scans the axis again to calculate velocity. The 
        calculated velocity and latest scan position are set as class objects.
        '''
        
        self.rtp.scanZ()
        
        if self.rtp.z == True:

            if self.axis == 1:
                self.rtp.scanX()
                self.x = self.rtp.x/1000
                
            else:
                self.rtp.scanY()
                self.x = self.rtp.y/1000

        else:
            self.x = 0
            self.x_dot = 0
            print('No ball detected')
        
 
    
    def get_EncData(self):
        ''' Gets theta and theta dot from the encoder. Gets the encoder position in ticks and converts it to degrees for a 
        4000 PPR encoder. Also calculates delta for the given interval. 
        get_EncData needs to be run twice at the input parameter interval to 
        provide an acurate theta dot.
        '''
   
        self.Enc.run()     
        
        self.theta = 2*self.pi*self.Enc.position/4000
        
        ## Class object for the encoder delta [in Radians per interval]
        self.delta = 2*self.pi*self.Enc.delta/4000
        
        
    def get_States(self):
        ''' Runs the encoder and rtp sensor twice at the given interval to find
        x, theta, x dot, & theta dot. The ball and angular velocities are calculated
        using the interval between the rtp scan and encoder being run.
        '''
        
        t0 = utime.ticks_ms()
        t1 = utime.ticks_add(t0, self.interval)
        
        self.get_EncData()
        self.get_rtpData()
        x0 = self.x
        
        Done = False
        
        while Done == False:
            
            if utime.ticks_ms() >= t1:
                self.get_EncData()
                self.get_rtpData()
                
                # Calculates x_dot in m/s for a ms interval scanX wait time [note mm/ms = m/s]
                self.x_dot = (self.x - x0)/self.interval
                
                # Calculates theta dot using the encoder delta over the inputed ms interval 
                self.theta_dot = self.delta/(self.interval/1000)
                
                Done = True
                
            else:
                pass
            

        
        
if __name__ == '__main__':
        
    xTask = balancingPlatform('x',10,1.5,0.8524,1,0.06845)
            
    while  True:

        xTask.run()
        utime.sleep_ms(50)
    