

'''
@file Lab4Plot.py
@brief This file plots the temperature data collected during Lab 4
@author Kai Quiozn
@date Febraury 11, 2021
'''

from matplotlib import pyplot as plt
import array

## Initialize Data arrays

time = array.array('f', [0])
Ta = array.array('f', [0])
Tc = array.array('f', [0])

#Open CSV File
ref_data = open('tempData.csv')

titles = ref_data.readline()

#Read CSV File Loop
while True:
    line = ref_data.readline()
    
    if line == '':
        break
    
    else:
        (s,minu,h,T_c,T_a) = line.strip().split(',')
        time.append(float(h))
        Tc.append(float(T_c))
        Ta.append(float(T_a))
        
    
#Convert to Lists
time_plot = list(time)
Tc_plot = list(Tc)
Ta_plot = list(Ta)

#Remove dead zeros
time_plot[0] = None
Tc_plot[0] = None
Ta_plot[0] = None

#Plot
plt.plot(time_plot, Tc_plot, 'b', time_plot, Ta_plot, 'r')
plt.title('Ambient and Core Reading Temperatures Over 9 Hours')
plt.xlabel('Time (s)')
plt.ylabel('Temperature (*F)')
plt.ylim([72, 86])
plt.legend(['Core Temperature', 'Ambient Temperature'])
