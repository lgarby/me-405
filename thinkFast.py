'''
@file thinkFast.py

@brief Reaction time testing class 

@details This class and file contain a finite state machine used to test a users reaction speed by turning on an LED and
timing how long it takes for the user to press the user button. The MCU X1 Crystal is used for timing, and external interrupts
are used to record reaction times for the most precise measurments possible. To finish testing, the user can perform a 
KeyboardInterrupt to stop the program and display their final average reaction time.

@author Logan Garby

@date January 27, 2020

'''

import pyb
import random
import utime

class thinkFast:
    '''
    @brief Class for reaction time testing program
    
    @details thinkFast contains the attributes and methods to perform reaction time testing. This class must be looped
    in the following statement:
        
    @code    try: 
        while True:
            task.run()
        except KeyboardInterrupt:
            print('Average Reaction Time = {:}'.format(task.sum/task.n))
    '''
    
    ## Integer attribute for State 0 
    S0_INIT = 0
    
    ## Integer attribute for State 1 
    S1_TESTING = 1
    
    ## Integer attribute for State 2 
    S2_RESULTS = 2
    
    def __init__(self):
        '''
        Initialization of all necessary thinkFast objects
        
        Notable objects include the MCU timer, LED pin, and user button pin, as well as the
        creation of an external interrupt oject triggered by the falling edge of the user button.
        
        '''
        ## Timer Attribute (set initially to 0)
        self.tim = pyb.Timer(5, prescaler=79, period=0x7FFFFFFF)
        self.tim.counter(0)
        
        ## LED Pin Object
        self.LED = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)
        self.LED.low()
        
        ## State Attribute
        self.state = self.S0_INIT
        
        ## Counter time attribute in ms
        self.time = 0
        
        ## Time at which next event occurs
        self.next_time = 0
        
        ## Time at which the LED turns on
        self.test_time = 0
        
        ## Reaction time in ms
        self.react_time = 0
        
        ## Time attribute for resetting program
        self.reset_time = 0

        ## String for initialization print message
        self.string = 'Press blue button when green light appears in 2-3s'
        
        ## Reset Indicator
        self.reset = False
        
        global react_time_us
        react_time_us = 0
        
        ## User Button Pin
        self.userPin = pyb.Pin.board.PC13
        self.userPin.PULL_UP
        
        ## External Interrupt Attribute
        self.extint = pyb.ExtInt(self.userPin, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, self.reactionTime)
    
        ## Number of Tests
        self.n = 0
        
        ## Sum of reaction times
        self.sum = 0
        
    def run(self):
        '''
        Runs one iteration of the thinkFast finite state machine. The run 
        method must be looped in a main file in order for the finite
        state machine to work. 
        '''
        self.time = self.tim.counter()/1000

        if(self.state == self.S0_INIT):
            
            
            if self.string != None:
                print(self.string)

                # Sets next_time to random ms value between 2s and 3s
                self.next_time = self.time + random.randrange(2000, 3000, 1)
                
                self.string = None
            else:
                pass
            
            # Transitions to test state once time = next_time
            if(self.time >= self.next_time):
                self.transitionTo(self.S1_TESTING)
                self.test_time = self.time
            else:
                pass
            
        if(self.state == self.S1_TESTING):
            
            self.LED.high()
            
            utime.sleep_ms(100)
            
            if react_time_us != 0:
                self.react_time = react_time_us/1000 - self.test_time
                self.LED.low()
                self.sum += self.react_time
                self.n += 1
                self.transitionTo(self.S2_RESULTS)
            else:
                pass
        
        if(self.state == self.S2_RESULTS):
            
            if(self.react_time != 0):
                print('{:} ms reaction time'.format(self.react_time))              
                self.next_time = self.time + 1000
                self.react_time = 0
            else:
                pass
        
            
            if(self.time >= self.next_time and self.reset == False):
                print('Test will reset in 5 seconds')
                self.reset_time = self.time + 5000
                self.reset = True
            else:
                pass
            
            if(self.time >= self.reset_time and self.reset == True):
                self.transitionTo(self.S0_INIT)
                self.resetFun()
            
    def reactionTime(self, line):
        ''' Callback for falling edge ISR. When triggered, the global variable
        react_time_us is set the the timer counter value.
        '''
        global react_time_us
        react_time_us = self.tim.counter()


    
    def resetFun(self):
        '''
        Resets all the required objects to re-run the test.
        '''
        self.tim.counter(0)
        self.LED.low()
        self.time = 0
        self.next_time = 0
        self.test_time = 0
        self.react_time = 0
        self.reset_time = 0
        self.string = 'Press blue button when green light appears in 2-3s'
        self.reset = False
        
        global react_time_us
        react_time_us = 0
        
        
    def transitionTo(self, next_state):
        '''
        This method transitions to the next state
        '''
        self.state = next_state        



if __name__ == '__main__':
    
    task = thinkFast()

try:    
    while True:
        task.run()
except KeyboardInterrupt:
    print('Average Reaction Time = {:}'.format(task.sum/task.n))

