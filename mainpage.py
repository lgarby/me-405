'''
@file mainpage.py


@mainpage


@section sec_intro Introduction
Welcome to my Doxygen Code Documentation for ME 405. This webpage contains a variety of different python and mechatronics projects. These projects implement
concepts such as classes, drivers, finite state machines, multitasking, interrupts, ADC, UART, and I2C. Labs 2 and up involve hardware such as the Nucleo STM32 
microprocessor, Adafruit MCP9808 temperature sensor, Adafruit FCP touch sensor with resistive touch panel, 12V DC motors, and hall sensor motor encoders. Labs
5-9 contain the files, calculations, and drivers used for the Mechatronic Balancing Board project. The picture of the project below is displayed better in @ref Lab8 and a link
to a video demonstration is given in @ref Lab9. Enjoy!



@image html IMG-4846.jpg "Mechatronic Balancing Board (Term Project)" width=400in


@section Labs Labs

Click the following links to view my ME 305 Lab Projects:
    @par
@ref Lab1
    @par
@ref Lab2
    @par
@ref Lab3
    @par
@ref Lab4
    @par
@ref Lab5
    @par
@ref Lab6
    @par
@ref Lab7
    @par
@ref Lab8
    @par
@ref Lab9


@section HW Homework

Click the following links to view my ME 405 Homeworks:
    @par
@ref HW0 
    
    

@page Lab1 Lab 0x01 - Reintroduction to Python and Finite State Machines

@section l1desc Description
This first project is a very simple re-introduction to Python. The vendingMachine class contains a finite state machine that controls the operation 
of a vending machine, including waiting for the user to input coins, make a selection, or eject their coins. The FSM also uses the keyboard module to 
check for keypresses without implementing blocking code. Refer to the figure below for more information about the design of the FSM. @ref FSM1
@par
@par
<b> Link to File Documentation: \b vendingMachine.py 
@par
<b> Link to Class Documentation: \b vendingMachine.vendingMachine

@section l1sc Source Code
<b> Lab 0x01 Source Code: \b https://bitbucket.org/lgarby/me-405/src/master/vendingMachine.py

@section FSM1 Finite State Machine Diagram

@image html Lab1_FSM.png



@page Lab2 Lab 0x02 - Think Fast!

@section l2desc Description
The goal of this project is to achieve extremely precise reaction time measurements. This is achieved by using the microcontroller Pierce oscilator
for timekeeping and external interrupts for the user's button press. The user is prompted to press the Nucleo STM-32 L476RG user input button when 
the green user LED lights up within 2-3 seconds. The reaction time is then recorded, displayed, and the program repeats itself after a slight pause. 
When finished, the user can perform a KeyboardInterrupt to stop the program which will then compute and display their average reaction time over 
the series of tests.
@par
<b> Link to File Documentation: \b thinkFast.py 
@par
<b> Link to Class Documentation: \b thinkFast.thinkFast

@section l2sc Source Code
<b> Lab 0x02 Source Code: \b https://bitbucket.org/lgarby/me-405/src/master/thinkFast.py



@page Lab3 Lab 0x03 - Pushing the Right Buttons

@section l3desc Description
The objective of Lab 3 is to use the ADC voltage reading capability of the Nucleo STM32 in conjunction with UART and serial capabilities to record 
voltage data in quick succession and transmit it to a PC for processing. In addition to using ADC and serial, this project also required running 
simultaneous tasks on the PC and Nucleo, as well as building arrays, plotting arrays, and creating .csv files. The final product is a program that,
when 'g' is pressed, records the voltage step response that occurs when the user button is released, plots it, and creates a .csv file of the data.

@section l3files Links to File Documentation:
@par
adcFrontEnd.py 
@par
adcDataCollection.py 
@par
405Lab3main.py 
@par
@section l3classes Links to Class Documentation:
@par
adcFrontEnd.adcFrontEnd
@par
adcDataCollection.adcDataCollection

@section l3sc Source Code
@par
<b> adcFrontEnd.py: \b https://bitbucket.org/lgarby/me-405/src/master/adcFrontEnd.py
@par
<b> adcDataCollection.py: \b https://bitbucket.org/lgarby/me-405/src/master/adcDataCollection.py
@par
<b> main.py: \b https://bitbucket.org/lgarby/me-405/src/master/405Lab3main.py
@par
<b> CSV file: \b https://bitbucket.org/lgarby/me-405/src/master/adcData.csv



@page Lab4 Lab 0x04 - Hot or Not?

@section l4desc Description
Lab 4 utilized an Adafruit MCP9808 Temperature Sensor Breakout Board to perform I2C communication with the STM32 Nucleo. In addition, the Nucleo itself 
applied the ADCAll object to access all ADC pins and read the MCU's core temperature. The objective of this project is to use the Nucleo and MCP9808 to 
take continuous temperature measurements of the environment and MCU core over long periods of time. These tasks were designed to occur solely on the Nucleo, 
and upon completion, be stored on the Nucleo as a .csv file. Class mcp9808.mcp9808 is a temperature sensor driver used by tempCollection.py 
to collect and store the data. Once the collection is complete and the .csv is written, ampy is used to retrieve the data for plotting.

@section colab Collaberation on Lab 4
Lab 4 was completed largely due to the efforts of fellow student and group partner Kai Quizon. Due to an abundance of temperature sensor issues experienced 
by myself, Kai completed the coding of Lab 4 nearly entirely on his own and before the deadline nonetheless. Although I am glad that I was eventually able to 
resolve the soldering and memory issues that occurred, much of the credit for the completion of this Lab is due to Kai.

@section l4files Hyperlinks to the Lab 4 File and Class Documentation
mcp9808.mcp9808 \n
tempCollection.py

@section l4sc Shared Repository Source Code Links
@par
<b> Shared Reposity: </b> https://bitbucket.org/kquizon/me-405/src/master/Lab4/ 
@par
<b> Personal Repository: </b> 
@par
<b> Sensor Driver (mcp9808.py): </b> https://bitbucket.org/lgarby/me-405/src/master/mcp9808.py  
@par
<b> Collection Task (tempCollection.py): </b> https://bitbucket.org/lgarby/me-405/src/master/tempCollection.py 
Note: these files are based on the ones in the shared reposity; however, they are altered based on my specific sensor and post-deadline work. 


@section png4 Temperature Collection Results
Data collection for the following plot began at 1:36 PM and was ended at 10:18 PM. As excepted for a house with no central A/C, the temperature 
steadily rose throughout the day. The sharp decline is due to the opening of a window allowing the cool evening air to steadily lower the 
temperature in the room.
@image html Lab4.png "Temperature Collection over a 522-minute span."



@page Lab5 Lab 0x05 - Getting Tipsy


@section l5desc Description
Lab 5 provides a much-needed break from coding with the task of solving the kinematics and equations of motions for the balancing platform
that will be used for the term project. The following diagrams and 3 pages of hand calculations show the steps and assumptions taken to relate
the motion of the balancing board motor to the angle of the platform and behavior of the ball.

@section l5ref Reference Diagrams
The following images assign symbols to the constant lengths and parameters that are used throughtout the calculations.

@image html Lab5_1.PNG "Balancing Board Parameters"
@image html Lab5_2.PNG "Lever Arm and Push Rod Parameter Definitions"

@section l5hc Hand Calculations
Using the symbols dictated above, the kinematics and equations of motion are solved for below:

@image html Lab5_3.jpg "Page (1/3)" width=700in
@image html Lab5_4.jpg "Page (2/3)" width=700in
@image html Lab5_5.jpg "Page (3/3)" width=700in



@page Lab6 Lab 0x06 - Simulation or Reality?

@section l6desc Description
Using the final equation of motion matrices from @ref Lab5 , Lab 6 involves creating a time-dependent simulation from those matrices. I used MATLAB 
to achieve the goal of this lab by first entering the non-linearized matrices, second linearizing them with a Jacobian linearization, and lastly 
running a state-space Simulink model to record and plot the predicted output of the model
@par

@section l6sc Links to MATLAB Source Code
@par
<b> MATLAB file:</b> https://bitbucket.org/lgarby/me-405/src/master/ME405_Lab6.m 
@par
\b Simulink:  https://bitbucket.org/lgarby/me-405/src/master/ME405_Lab6sim.slx

@section l6r Results

@image html Lab6_OLa.png "Figure 1 - Part a. Simulated the ball and platform both at rest at their equilibriums." 
\n
@image html Lab6_OLb.png "Figure 2 - Part b. of the open loop lab assignment. The ball is simulated as being placed 5cm from equilibrium."
\n
@image html Lab6_OLc.png "Figure 3 - Part c. in open loop configuration. Here the ball is at the equilibruim but the platform begins tilted at 5 degrees."
\n
@image html Lab6_OLd.png "Figure 4 - Part d. displays the open loop response of the ball and platform when acted upon by a 1 mNm-s impulse from the motor."
\n
@image html Lab6_CL.png "Figure 5 - Closed Loop response of the system when the ball is placed 5cm from equilibrium such as in Figure 2."


@page Lab7 Lab 0x07 - Feeling Touchy

@section lab7desc Description
Lab 7 implemented a resistive touch panel to the balancing board project. The task for this assignment was to create a driver for the 8-inch resistive touch panel, 
which uses resistor voltage dividers to measure the position of a contact point. The difficult aspect of this lab is that only one axis position can be measured
at a time since the pin modes for these measurements and voltage outputs differ depending on the axis of the scan. rtpDriver.rtpDriver constantly re-initializes 
the pins connected to the AdaFruit FCP Touch Sensor breakout board in order to complete the series of scans that returns a defined positional state.

@section l7hls Documentation Hyperlinks
@par
Class Documentation: rtpDriver.rtpDriver
@par
File Documentation: rtpDriver.py

@section l7sc Link to Source Code
@par
<b> rtpDriver Source Code: </b>: https://bitbucket.org/lgarby/me-405/src/master/rtpDriver.py

@section l7ex Example of REPL Results
This data was taken using the main script at the bottom of the rtpDriver.py file. The getRtpData() method is run every two seconds and the tuple of positions is
returned along with the time taken by the x scan and total scan time. The following data was taken by pressing as close to the center of the board as possible.
@code
(-0.09230769, 0.3333333, False)
500 microsecond x scan
1449 microseconds
(0.09230769, 0.5238095, False)
501 microsecond x scan
1449 microseconds
(0.09230769, 0.4761905, False)
500 microsecond x scan
1449 microseconds
@endcode

@section l7images Images of the Lab Setup
@image html IMG-4733.jpg "Balancing Board Setup" width=500in
@image html IMG-4735.jpg "FCP Touch Sensor Breakout Board" width=500in


@page Lab8 Lab 0x08 - Term Project I

@section l8desc Description 
The purpose of this lab was to design a motor driver and motor encoder. These two drivers will work together to determine the position of the motor 
and change to position of the motor. The bulk of the MotorDriver.py code was written during the lab for ME305. MotorDriver.py works by controlling 
the speed of the motor with PWM. MotorEncoder.py works by using a timer that counts in ticks, which can be converted to an angular position. 
lab0x03share.py may not be needed for this lab assignment but will be useful when we need to combine various drivers for the term project.
* <b>This Lab was created by Brandon Halebsky, Rebecca Rodriguez, and Logan Garby </b> 


@section l8hls Documentation Hyperlinks
@par
Motor Driver Class Documentation: Motor_Driver.Motor_Driver
@par
Motor Driver File Documentation: Motor_Driver.py
@par
Encoder Driver Class Documentation: Motor_Encoder.Motor_Encoder
@par
Encoder Driver File Documentation: Motor_Encoder.py

@section l8sc Source Code
@par
<b> MotorDriver Source Code: </b>: https://bitbucket.org/rrodri98/term-project-part-i/src/main/ME305%20Drivers%20-%20Brandon/MotorDriver.py
@par
<b> MotorEncoder Source Code: </b>: https://bitbucket.org/rrodri98/term-project-part-i/src/main/ME305%20Drivers%20-%20Brandon/MotorEncoder.py
@par
<b> Shares File Source Code: </b>: https://bitbucket.org/rrodri98/term-project-part-i/src/main/ME305%20Drivers%20-%20Brandon/lab0x03share.py

@section l8pic Image of Setup 
@image html IMG-4846.jpg "Balancing Board Motors and Encoders" width=700in


@page Lab9 Lab 0x09 - Term Project II

@section l9desc Description
Lab 9 served as the culmination of two quarters of mechatronics classes. My partner Brandon and I were successfully able to balance a ball on the platform
using the control theory derived in @ref Lab5 and @ref Lab6, the drivers created in @ref Lab6 and @ref Lab7, and the methods for finding controller gains 
described in lecture. Overall, the balancing of the system was a success; however, there are many improvements that could be made for yielding better performance.
Mostly, the controller gains could use more experimentation in order to find a more stable and responsive solution. Due to the state of the system containing four 
variables for each axis, there was a certain amount of guessing and checking used to find a suitable gain. In addition, the axes were assumed to be independent of
each other with is not entirely true, and the system could have possibly benefited from using an eight-state representation. Despite the limitations of the activity,
it was still a successful project for our team and we are excited to have been able to achieve the desired outcome.
* <b>This project was created by Brandon Halebsky and Logan Garby </b> 

@section l2v Link to Video

* https://youtu.be/S8OiUb8cOJ8


@section l9hls Documentation Hyperlinks
@par
Main File Documentation: main.py

@par
Balancing Platfrom Class: balancingPlatform.balancingPlatform
@par
Balancing Platfrom File: balancingPlatform.py

@par
Motor Driver Class Documentation: Motor_Driver.Motor_Driver
@par
Motor Driver File Documentation: Motor_Driver.py

@par
Encoder Driver Class Documentation: Encoder_Driver.Encoder_Driver
@par
Encoder Driver File Documentation: Encoder_Driver.py

@par
Resistive Touch Panel Class Documentation: rtpDriver.rtpDriver
@par
Resistive Touch Panel File Documentation: rtpDriver.py

@section l9sc Links to Shared Repository Source Code
    @par
<b> Main File: </b> https://bitbucket.org/rrodri98/term-project-part-i/src/main/Part%20II%20Files/main.py
    @par
<b> Balancing Platform: </b> https://bitbucket.org/rrodri98/term-project-part-i/src/main/Part%20II%20Files/balancingPlatform.py
    @par
<b> Motor Driver: </b> https://bitbucket.org/rrodri98/term-project-part-i/src/main/Part%20II%20Files/Motor_Driver.py
    @par
<b> Encoder Driver: </b> https://bitbucket.org/rrodri98/term-project-part-i/src/main/Part%20II%20Files/Encoder_Driver.py
    @par
<b> Resistive Touch Panel Driver: </b> https://bitbucket.org/rrodri98/term-project-part-i/src/main/Part%20II%20Files/rtpDriver.py
    @par



@page HW0 Homework 0x01 - Python Review (Functions/Tuples)

@section hw0desc Description
This project is a function that takes a price input and payment tuple input and returns the proper amount of change.

@section h1sc Source Code
<b> Lab 0x01 Source Code: \b: https://bitbucket.org/lgarby/me-405/src/master/ME405_HW0x01.py












'''

