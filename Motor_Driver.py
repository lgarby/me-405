'''
@file Motor_Driver.py

@brief Initialized pins, timers, and channels for motor control

@details This class needs string inputs for the sleep pin, in1 pin/channel, in2 
pin/channel, and timer, which it uses to initialize the necessary objects for
controlling one motor using pulse width modulation. The methods avalible are, \
enable(), disable(), and set_duty(). In addition, an external interupt is
placed on the nFAULT pin to stop motion if a fault is detected.

@author Logan Garby

@date 3/17/2021
'''

import pyb
import utime

class Motor_Driver:
    '''
    @brief Enables, disables, and sets motor duty cycle
    
    @details Class Motor_Driver, initializes objects and contains enable, disable, and
    set duty methods. It also contains a callback the stop all motor function 
    that is triggered by an external interupt which happens when the motor
    driver detects a fault on the B2 pin.
    '''
    
    def __init__(self,nSLEEP_pin, nFAULT_pin, IN1_pin, IN1_channel, IN2_pin, IN2_channel, timer):
        ''' Initializes pins, timer, and channels. Also sets up interupt by initializing it, 
        disabling the interupt, enabling the motor sleep pin, and then re-enabling the interupt.
        This process must be followed whenever initializing these motors.
        
        @param nSLEEP_pin nSLEEP pin string input
        @param nFAULT_pin
        @param IN1_pin IN1 pin string input
        @param IN1_channel Timer cannel for IN1 pin
        @param IN2_pin IN2 pin string input
        @param IN1_channel Timer channel for IN2 pin
        @param timer Timer number
        '''
        
        ## nSLEEP pin object (enables and disables motor)
        self.nSLEEP = pyb.Pin (nSLEEP_pin, pyb.Pin.OUT_PP)
        
        ## nFAULT pin object
        self.nFAULT_pin = pyb.Pin (nFAULT_pin)
        # self.nFAULT_pin.PULL_UP
        
        ## External Interupt Object
        try:
            self.extInt= pyb.ExtInt(self.nFAULT_pin, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP,  self.faultStop)
        except ValueError:
            pass
        
        ## IN1 pin attribute
        self.IN1_pin = pyb.Pin(IN1_pin)
        
        ## IN2 pin attribute
        self.IN2_pin = pyb.Pin(IN2_pin)
        
        ## Timer attribute - set at frequency of 20 kHz
        self.timer = pyb.Timer(timer, freq=20000)
        
        ## IN1 PWM attribute
        self.IN1 = self.timer.channel(IN1_channel, pyb.Timer.PWM, pin= self.IN1_pin)
        
        ## IN2 PWM attribute
        self.IN2 = self.timer.channel(IN2_channel, pyb.Timer.PWM, pin= self.IN2_pin)
        
        '''
        Initializing IN1 and IN2 to 0% duty cycle and setting nSLEEP low
        '''
        self.IN1.pulse_width_percent(0)
        self.IN2.pulse_width_percent(0)      
        self.nSLEEP.low()
        
        
    def enable(self):
        '''
        Enables nSLEEP pin.
        '''
        self.nSLEEP.high()
        
        
    def disable(self):
        '''
        Disables nSLEEP pin
        '''
        self.nSLEEP.low()
        
        
    def faultStop(self,line):
        ''' This disables the motor when the current limit is reached. 
        External interupt callback. This method provides the callback for the 
        external interupt used for fault detection. If a fault is detected, this 
        callback disables the motor and waits until the fault is no longer detected
        before exiting the callback.
        '''
        self.disable()
        
        print('Motor Stopped for Safety')
        
        self.extInt.disable()
        
        # nFAULT = True
        
        # while nFAULT == True:
            
        #     # nFAULT pin reads 0 if fault is present, 1 if there is no fault.
        #     nFAULT_read = self.nFAULT_pin.value()
        
        #     if nFAULT_read == 0:
                
        #         # Waits 10 ms before re-checking if the fault is present
        #         utime.sleep_ms(10)
            
        #     elif nFAULT_read == 1:
                
        #         nFAULT = False
                
        #     else:
        #         pass
        
        
    def reset(self):
        '''
        Sets all pin voltages to 0V
        '''
        self.nSLEEP.low()
        
        self.IN1.pulse_width_percent(0)
        
        self.IN2.pulse_width_percent(0)
        
        
    def set_duty(self, duty):
        ''' Sets motor duty cycle. Sets duty cycle percentage to inputed 
        value between -100 and 100. Negative duty cycle inputs will reverse 
        the motor direction.

        '''
        ## Duty Cycle Attribute
        self.duty = duty
        
        if self.duty < 0:
            '''
            For negative duty cycle inputs
            '''
            self.IN1.pulse_width_percent(0)
            self.IN2.pulse_width_percent(-self.duty)
            
        elif self.duty >= 0:
            '''
            For positive duty cycle inputs
            '''
            self.IN1.pulse_width_percent(self.duty)
            self.IN2.pulse_width_percent(0)
        else:
            pass
        
        
        
if __name__ == '__main__':

    '''
    This section tests the code using UART. 'e' Enables the motor, 'd' 
    disables it, and a number 0-9 will set the duty cycle to 10 times that 
    value.
    '''

    # Creates pin objects used to interface with the motor driver
    pin_nSLEEP = 'A15'
    pin_nFAULT = 'B2'
    pin_IN1 = 'B0'
    pin_IN2 = 'B1'

    # Creates timer object used for PWM
    tim = 3
    
    IN1_channel = 3
    IN2_channel = 4
    
    # Creates motor object for passing in pins and timer
    M1 = Motor_Driver(pin_nSLEEP, pin_nFAULT, pin_IN1, IN1_channel, pin_IN2, IN2_channel, tim)
    M2 = Motor_Driver(pin_nSLEEP, pin_nFAULT, 'B4', 1, 'B5', 2, tim)

    M1.disable()
    M1.set_duty(100)
    M2.set_duty(0)
    M1.extInt.disable()
    M1.enable()
    utime.sleep_ms(100)
    M1.extInt.enable()

    
    while True:
        try:
            utime.sleep_ms(10)
        except KeyboardInterrupt:
            M1.reset()
            M2.reset()
            break
    
    
    
                

