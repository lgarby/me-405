# -*- coding: utf-8 -*-
## @file MotorEncoder.py
#  MotorEncoder.py acts as a driver for a motor encoder.
#
#  A MotorEncoder object uses the run() method to keep track of the position of the motor shaft. It does this through the use of a finite state machine
#  and a file lab0x03share.py. lab0x03share.py is used to store variables used by multiple files.
#
#  @author Brandon Halebsky
#
#  @copyright License Info
#
#  @date 2/10/2021
#
#  @package MotorEncoder.py
#  MotorEncoder.py acts as a driver for a motor encoder.
#
#  A MotorEncoder object uses the run() method to keep track of the position of the motor shaft. It does this through the use of a finite state machine
#  and a file lab0x03share.py. lab0x03share.py is used to store variables used by multiple files.
#
#  @author Brandon Halebsky, Logan Garby, Rebecca Rodriguez
#
#  @copyright License Info
#
#  @date 3/10/2021
import pyb
import lab0x03share


class MotorEncoder:
    '''
    @brief      A class which determines the position of a motor encoder.
    @details    This class implements a finite state machine to keep track of 
                the position the motor encoder is in. First, the change in 
                motor position is calculated to avoid period overflow, then the
                change in position is either added or subtracted to an ongoing
                account of the position to create an updated position. 
    '''
    ## Contant defining State 0 - Initialization
    S0_INIT = 0
    ## Constant defining State 1 - Call Update
    S1_CallUpdate = 1
    
    def __init__(self,dont_init=1,):
        '''
        @brief  Creates a MotorEncoder object.
        @details Initialized objects for the encoder pins, timers, and channels. if dont_init
        is set to 1, this constructor will not return the encoder position and count
        attributes back to zero.
        '''
        ## Object for do or don't initialize boolean
        self.dont_init =dont_init 
        #prevents init method from running repeatedly causing delta and position to be constantly reset
        if(self.dont_init):
            lab0x03share.rolls = 0    # init rollover count to 0
            lab0x03share.raw_pos = 0  # init initial raw position to 0
            #sets initial state as S0_Init
            ## Attribute for the current state of the FSM
            self.state = self.S0_INIT
            
            #initializes timer and motor encoder on the hardware side of things
            lab0x03share.tim = pyb.Timer(4)
            lab0x03share.tim.init(prescaler=0,period=0xFFFF)
            lab0x03share.tim.channel(1,pin=pyb.Pin.cpu.B6,mode=pyb.Timer.ENC_AB)
            lab0x03share.tim.channel(2,pin=pyb.Pin.cpu.B7,mode=pyb.Timer.ENC_AB)
            
            
            
            #intializes position and delta variables for the first run
            #in otherwords syncronizes position variable (software) and motorEncoder (hardware)
            lab0x03share.position = 0
            #print('init position')
            lab0x03share.raw_delta = 0
            
            #sets up variables which help with counting negative numbers
            ## 
            # self.positionHold = None
            # self.x=None
        
    def run(self):
        '''
        @brief      Runs one iteration of the task run.
        @details    Provides start up variables, then
                    runs the method update() in order to sync software and hardware.
        '''
        
        #Initialization, sets initial position of motor encoder to zero
        if self.state == self.S0_INIT:
            lab0x03share.rolls = 0    # init rollover count to 0
            lab0x03share.raw_pos = 0  # init initial raw position to 0
            #print ('State 0 - Initializing')
            self.update()
            self.transitionTo(self.S1_CallUpdate)
            lab0x03share.position = 0
        
        # CallUpdate runs the method update() to sync software and hardware  
        elif self.state == self.S1_CallUpdate:
            #print('State 1 - Calling Updates')
            self.update()
            #print('problem here?')
            self.get_position()
            
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run.
        @param newState  An integer defining the state to transition to.
        '''
        self.state = newState
        
    def update(self):
        '''
        @brief      This method updates the position of the motor shaft.
        @details    This does not need to be used by the average user, the user should use the methodn run().
        '''

        # Read in new raw position and calculate a delta from the previous position.
        lab0x03share.raw_pos_previous = lab0x03share.raw_pos   # Save previous position (before postion gets overwritten with new value.)
        lab0x03share.raw_pos = lab0x03share.tim.counter()      # Read in raw value of current position of motor.
        lab0x03share.raw_delta = lab0x03share.raw_pos - lab0x03share.raw_pos_previous

        # Check for rollovers:
        #   The idea is there will be a BIG numeric difference in current raw position
        #   compared to the previous raw position. Depending on the sign of this difference
        #   will determine whether the rollover count (rolls) will increase or decrese. For each
        #   rollover, we will add +/-0x10000 to the raw position to determine the
        #   true position.
        if(lab0x03share.raw_delta < -0x10000/2):   # Check for CW direction rollover     
            lab0x03share.rolls = lab0x03share.rolls + 1
        elif(lab0x03share.raw_delta > 0x10000/2):   # Check for CCW direction rollover     
            lab0x03share.rolls = lab0x03share.rolls - 1
        
        lab0x03share.prev_position = lab0x03share.position
        lab0x03share.position = lab0x03share.raw_pos + (lab0x03share.rolls * 0x10000) # true position = raw position plus combined rollover effects
        lab0x03share.real_delta = lab0x03share.position - lab0x03share.prev_position
        
     
        
        
    def get_position(self):
        '''
      @brief Used for testing by printing position variable.

        '''
        #print('-----')
        #print(lab0x03share.position, '3')
        #print('-----')
        #y=1
        #print('positioned')
        #y=None
    
    
    
    def get_position1(self):
        '''
        @brief This method prints out the position variable.
        @details Used with UI.py file to allow for user input.
        '''
        print(lab0x03share.position)
        
        
        
    def set_position(self,new_position):
        '''
        @brief Sets position to a User specified position.
        @details Currently not implemented.

        '''
        lab0x03share.position = new_position
        lab0x03share.tim.counter(new_position)
        
    def zero(self):
            '''
           @brief Sets position of both software and hardware to 0.
           

        '''
            lab0x03share.position = 0
            lab0x03share.tim.counter(0)
            print('zeroed')
        
    def get_delta(self):
        '''
        @brief Prints out the delta value at that instant, if manually turning encoder this will most likely be 0.
       
        '''
        print(lab0x03share.real_delta)
        print('deltaed')
        

# THIS IS FOR TESTING NEW ENCODERS        
delta_track = [0]   
   
if __name__ =='__main__':
    task1 = MotorEncoder()
     
    print('task started')
    for n in range (100000):
        print(n)
        
        task1.run()
        print(lab0x03share.position, ' - Position')
        print(lab0x03share.real_delta, ' - Delta')
        delta_track.append(lab0x03share.real_delta)