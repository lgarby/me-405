'''
@file rtpDriver.py 

@brief File containing rtpDriver.rtpDriver class and test script.

@details This file is primariy for the resistive touch panel driver which 
implements an Adafruit FCP Touch Sensor breakout board to measure and set 
voltages throughout the touch screen. the rtpDriver.rtpDriver class uses these 
voltages to determine the x and y position of the contact.

@author Logan Garby

@date Mar 4th, 2021
'''


import pyb
import utime

class rtpDriver:
    '''
    @brief Resistive Touch Panel Driver Class
    @details This class contains methods to scan the x, y, and z axes and 
    determine the x, y positions as well as the z contact/no contact boolean. 
    This driver will be implemented in the balancing board term project to 
    determine the position and horizontal velocities of the rubber ball.
    '''
    def __init__(self,pin1,pin2,pin3,pin4,xticks2mm,yticks2mm,xcenter,ycenter):
        ''' Initalizing pin object for the FCP resistive touch sensor and gathering
        input on board dimensions. Balancing board touch sensor pins should 
        correspond to the following input parameters: 
            * 'A1' for pin1
            * 'A7' for pin2
            * 'A0' for pin3
            * 'A6' for pin4 
        
        Analog ticks are used because they are easy to empirically determine 
        and will be more accurate than guessing screen properties
        
        @param pin1 Touch sensor x_m pin [string] example: 'A1'
        @param pin1 Touch sensor x_p pin [string] example: 'A7'
        @param pin1 Touch sensor y_m pin [string] example: 'A0'
        @param pin1 Touch sensor y_p pin [string] example: 'A6'
        @param xticks2mm Amount of analog ticks per mm in the x dirrection 
            [float] example value: 21 (ticks/mm)
        @param yticks2mm Amount of analog ticks per mm in the y dirrection 
            [float] example value: 32.5 (ticks/mm)
        @param xcenter x-axis center of the board in analog ticks [int] 
            example value: 2014
        @param ycenter y-axis center of the board in analog ticks [int] 
            example value: 1920

        
        '''
        ## String class object for user inputed x_m pin
        self.xm_pin = pin1
        
        ## String class object for user inputed x_p pin
        self.xp_pin = pin2
        
        ## String class object for user inputed y_m pin
        self.ym_pin = pin3
        
        ## String class object for user inputed y_p pin
        self.yp_pin = pin4
        
        ## Class object for input of x direction touch panel ADC ticks per mm 
        self.dx = xticks2mm
        
        ## Class object for input of y direction touch panel ADC ticks per mm 
        self.dy = yticks2mm
        
        ## Class object for the center of the x axis (given in ADC ticks)
        self.xcenter = xcenter
        
        ## Class object for the center of the y axis (given in ADC ticks)
        self.ycenter = ycenter
        
        ## x position in mm
        self.x = 0
        
        ## y position in mm
        self.y = 0
        
        ## Value for ADC measurement during scanZ()
        self.zscan = 0
        
        ## z axis Boolean: True => Contact / False => No Contact
        self.z = None
        
        ## x, y, & z data tuple of the form (x [float],y [float],z [boolean])
        self.data = (0,0,None)

        
    def scanX(self):
        ''' Scans for touch contact in the x direction.
        Reads resistive touch panel's x position by setting x_p high, x_m low, 
        floating y_m and reading the value of y_p. xcenter and dx are used in
        this method to convert the ADC measurement into mm.
        ''' 
        ## Class object for the x_m pin (reinitialized every new scan)
        self.xm = pyb.Pin(self.xm_pin, mode=pyb.Pin.OUT_PP)
        
        ## Class object for the x_p pin (reinitialized every new scan)
        self.xp = pyb.Pin(self.xp_pin, mode=pyb.Pin.OUT_PP)
        
        ## Class object for the y_m pin (reinitialized every new scan)
        self.ym = pyb.Pin(self.ym_pin, mode=pyb.Pin.IN)
        
        ## Class object for the y_p pin (reinitialized every new scan)
        self.yp = pyb.ADC(self.yp_pin)
        
        self.xm.low()
        self.xp.high()
        self.ym.low()
        
        self.x = (self.yp.read()-self.xcenter)/self.dx
        
        ## Time at which the x scan is completed
        self.xtime = utime.ticks_us()
        
        
    def scanY(self):
        ''' Scans for touch contact in the y direction.
        Reads resistive touch panel's x position by setting x_p high, x_m low, 
        floating y_m and reading the value of y_p. ycenter and dy are used here
        to convert the ADC measurment to mm. '''
        self.xm = pyb.Pin(self.xm_pin, mode=pyb.Pin.IN)
        self.xp = pyb.ADC(self.xp_pin)
        self.ym = pyb.Pin(self.ym_pin, mode=pyb.Pin.OUT_PP)
        self.yp = pyb.Pin(self.yp_pin, mode=pyb.Pin.OUT_PP)
        
        self.xm.low()
        self.ym.low()
        self.yp.high()
        
        self.y = (self.xp.read()-self.ycenter)/self.dy
        
        
    def scanZ(self):
        ''' Scans z direction for any RTP contact.
        Energizes both resistor dividers to check for voltage at the center 
        node. Pin x_p reading high or low implies there is no contact and 
        z is set to false, if the value of x_p is within 100 ADC ticks of 
        0 or 4095, there is contact and z is set as true.
        '''
        self.xm = pyb.Pin(self.xm_pin, mode=pyb.Pin.OUT_PP)
        self.xp = pyb.ADC(self.xp_pin)
        self.ym = pyb.Pin(self.ym_pin, mode=pyb.Pin.OUT_PP)
        self.yp = pyb.Pin(self.yp_pin, mode=pyb.Pin.OUT_PP)
        
        self.xm.low()
        self.ym.low()
        self.yp.high()
        
        self.zscan = self.xp.read()
        
        if 100 < self.zscan and self.zscan < 3995:
            self.z = True
        else:
            self.z = False
            
            
    def getRtpData(self):
        ''' Runs all 3 axis data collection methods and returns a tuple in the 
        form (X, Y, Z). A utime ticks_us time object is also calculated to 
        determine how fast the three scans are running.
        '''
        self.scanX()
        self.scanY()
        self.scanZ()
        
        ## Time at which all scans are completed
        self.time = utime.ticks_us()
        
        self.data = (self.x,self.y,self.z)
        
        return self.data
        
        
if __name__ == '__main__':
    
    rtp = rtpDriver('A1','A7','A0','A6',21,32.5,2014,1920)
    
    while True:
        t0 = utime.ticks_us()
        rtp.getRtpData()
        run_time = rtp.time - t0
        print(rtp.data)
        print('{:} microseconds - x scan'.format(rtp.xtime-t0))
        print('{:} microseconds'.format(run_time))
        utime.sleep(2)
 
        
